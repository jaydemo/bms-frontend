import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Home from './components/home/Home';
import ApplyLoan from './components/ApplyLoan/ApplyLoan';
import Login from './components/login/Login';
import Header from './components/navbar/Header';
import Register from './components/register/Register';
import Welcome from './components/welcome/Welcome';
import ViewLoans from './components/ViewLoans/ViewLoans';
import CustomerDetail from './components/Customer/CustomerDetail';
function App() {
  return (
    <div>
      <BrowserRouter>
        <Header/>
        <Routes>
          <Route path='/register' element={<Register/>}/>
          <Route path='/login' element={<Login/>}/>
          <Route path='/welcome' element={<Welcome/>}/>
          <Route path='/' element={<Home/>}/>
          <Route path='/applyloan' element={<ApplyLoan/>}/>
          <Route path='/viewloans' element={<ViewLoans/>}/>
          <Route path='/customerDetail' element={<CustomerDetail/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
