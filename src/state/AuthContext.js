import React, { useEffect, useState } from "react";

const AuthContext = React.createContext({
  isLoggedIn: false,
  onLogout: () => {},
  onLogin:(boolVal)=>{},
  token:null
});

export const AuthContextProvider = (props) => {
    const [isLoggedIn,setIsLoggedIn] = useState(false);
    const [token,setToken]=useState('');
    const validationUrl = 'http://localhost:9090/validate';

    //logout action
    const logoutHandler = ()=>{
        localStorage.removeItem('token')
        setIsLoggedIn(false);
        console.log('logout successful');
    }
    //API call for login verification is need in futher development
    useEffect(()=>{
      if(localStorage.getItem('token'))
        setIsLoggedIn(true)
        console.log('user is logged in : authcontext');
    },[isLoggedIn])

  return <AuthContext.Provider value={{
    isLoggedIn: isLoggedIn,
    onLogout: logoutHandler,
    onLogin: setIsLoggedIn,
    token,
    addToken:setToken
  }}>{props.children}</AuthContext.Provider>;
};

export default AuthContext;