import { useContext} from 'react';
import { Button } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {NavLink, useNavigate } from 'react-router-dom';
import AuthContext from '../../state/AuthContext';
import './NavBar.css';
function Header(props) {
    const authContext = useContext(AuthContext);
    const navigate = useNavigate();
    function logout(){
        authContext.onLogout();
        navigate('/');
    }
    return (
        <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
            <Container>
                <Navbar.Brand><NavLink to='/' className='link-decoration'>Bank Management System</NavLink></Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    {
                        authContext.isLoggedIn?
                        <>
                        <Nav className='me-auto'>
                        <NavLink className='link-decoration' to="/customerDetail">Profile</NavLink>
                        <NavLink className='link-decoration' to="/viewloans">Loans</NavLink>
                        <NavLink className='link-decoration' to="/applyloan">Apply Loan</NavLink>
                        </Nav>
                        <Nav>
                        <Button variant="primary" type="submit" onClick={logout}>
                            Logout
                        </Button>
                         </Nav>
                        </>:<>
                    <Nav className="me-auto">
                        <NavLink className='link-decoration' to="/register">Register</NavLink>
                    </Nav>
                    <Nav><NavLink to='/login'>
                        <Button variant="primary" type="submit">
                            Login
                        </Button>
                    </NavLink>
                    </Nav>
                        </>
                    }
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default Header;