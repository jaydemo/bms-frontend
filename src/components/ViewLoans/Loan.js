const Loan = (props)=>{
    const loan = props.loanDetails;
    return<tr>
            <td>{loan.loanId}</td>
            <td>{loan.loanType}</td>
            <td>{loan.loanAmount}</td>
            <td>{loan.appliedDate}</td>
            <td>{loan.rateOfInterest}</td>
            <td>{loan.loanDuration}</td>
            <td>{loan.status}</td>
        </tr>
}

export default Loan;