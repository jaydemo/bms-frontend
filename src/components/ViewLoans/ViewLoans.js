import axios from "axios";
import { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import Loan from "./Loan";

function ViewLoans(){
    const token = localStorage.getItem('token');
    const viewLoanurl = 'http://localhost:9010/viewloan';
    const [loanDetails,setLoanDetails]=useState([]);
    useEffect(()=>{
    axios({
        headers:{
            Authorization:token,
            "Content-Type": "application/json"
        },
        url:viewLoanurl,
        method:'get'
    }).then(res=>setLoanDetails([...res.data]),err=>console.log('failure'));
    },[token,setLoanDetails]);

    console.log(loanDetails)
    return<Table striped hover>
        <thead>
            <tr>
                <th>Loan Id</th>
                <th>Loan Type</th>
                <th>Loan Amount</th>
                <th>Apply Date</th>
                <th>Rate Of Interest</th>
                <th>Loan Duration</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            {loanDetails.map(loan=><Loan loanDetails={loan}/>)}
        </tbody>
    </Table>
};

export default ViewLoans;