import { useState, useEffect } from "react";
import classes from "./Customer.module.css";
import axios from "axios";
function CustomerDetail() {
  const token = localStorage.getItem("token");
  const username = localStorage.getItem("username");
  const [details, setDetails] = useState();
  const customerDetailUrl =
    "http://localhost:9090/loginapp/finduser/" + username;
  console.log("customer details running.....");
  useEffect(() => {
    (async () => {
      try {
        let response = await axios({
          headers: {
            Authorization: token,
            "Content-Type": "application/json",
          },
          url: customerDetailUrl,
          method: "get",
        });
        console.log("response is : ", response.data);
        setDetails(response.data);
      } catch (err) {
        console.log(err.message);
      }
    })();
  }, [setDetails, customerDetailUrl, token]);
  if (!details) return <p>couldn't find any details</p>;
  return (
    <div className={classes.userInfo}>
      <ul>
        <li>Username : {details.username}</li>
        <li>Name : {details.name}</li>
        <li>Address : <input type="text" value={details.address} name="Address" /></li>
        <li>State : <input type="text" value={details.state} name="State" /></li>
        <li>Country : <input type="text" value={details.country} name="Country" /></li>
        <li>Email : <input type="text" value={details.email} name="Email" /></li>
        <li>Contact Number : <input type="text" value={details.contactNumber} name="Contact Number" /></li>
        <li>Account Type : <input type="text" value={details.accountType} name="Account Type" /></li>
        <li>PAN : <input type="text" value={details.pan} name="PAN" /></li>
        <li>Date Of Birth : <input type="date" value={details.dob} name="state" /></li>
      </ul>
    </div>
  );
}

export default CustomerDetail;
