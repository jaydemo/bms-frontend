import { Container } from "react-bootstrap";

export default function Home(){
    return(
        <Container>
            <h1>This is our home page</h1>
        </Container>
    )
}