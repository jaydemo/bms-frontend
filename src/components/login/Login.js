import axios from "axios";
import { useState, useContext } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { NavLink, useNavigate } from "react-router-dom";
import "./Login.css";
import AuthContext from "../../state/AuthContext";
function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [token, setToken] = useState("");
  const navigate = useNavigate();
  const [valid, setValidity] = useState(false);
  const authContext = useContext(AuthContext);
  const loginURL = "http://localhost:9090/loginapp/login";
  async function submit(e) {
    let credentials = { username, password };
    console.log(username);
    console.log(password);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.preventDefault();
      e.stopPropagation();
    }
    setValidity(true);
    e.preventDefault();
    axios({
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      url: loginURL,
      data: JSON.stringify(credentials),
    })
      .then(
        (res) => saveInLocal(res.data),
        console.log("did not receive any token")
      )
      .catch();
    console.log("token is ", token);
  }
  const saveInLocal = (res) => {
    setToken(res.authToken);
    console.log("inside save in local", res);
    authContext.onLogin(true);
    localStorage.setItem("token", res.authToken);
    localStorage.setItem('username',res.username);
    navigate("/welcome");
  };

  if (localStorage.getItem("token")) {
    authContext.onLogin(true);
    navigate("/");
  }
  return (
    <div className="offset-sm-3 login-component">
      <Form noValidate validated={valid} onSubmit={submit}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Username</Form.Label>
          <Form.Control
            type="text"
            placeholder="Username"
            onChange={(e) => setUsername(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please enter your username
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            onChange={(e) => {
              setPassword(e.target.value);
              console.log(password);
            }}
          />
          <Form.Control.Feedback type="invalid">
            please enter your password
          </Form.Control.Feedback>
        </Form.Group>
        <Button variant="primary" type="submit" className="w-100">
          Login
        </Button>
      </Form>
      <div style={{ paddingTop: "1%" }}>
        New User?
        <NavLink to="/register" className="link-decoration-login text-primary">
          Register here
        </NavLink>
      </div>
    </div>
  );
}

export default Login;