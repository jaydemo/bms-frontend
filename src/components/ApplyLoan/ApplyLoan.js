import { useState } from "react";
import Card from "../UI/Card";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import axios from "axios";
export default function ApplyLoan() {
  const userName = localStorage.getItem("username");
  const [loanType, setLoanType] = useState("");
  const [loanAmount, setLoanAmount] = useState("");
  const [appliedDate, setAppliedDate] = useState("");
  const [rateOfInterest, setRateOfInterest] = useState(0);
  const [loanDuration, setLoanDuration] = useState("");
  const [valid, setValidity] = useState(false);
  function submitLoan(e) {
    const token = localStorage.getItem("token");
    const applyLoanUrl = "http://localhost:9010/applyloan";
    let loanDetails = {
      userName,
      loanType,
      loanAmount,
      appliedDate,
      rateOfInterest,
      loanDuration,
    };
    console.table(loanDetails);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.preventDefault();
      e.stopPropagation();
    }
    setValidity(true);
    axios({
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
      method: "post",
      url: applyLoanUrl,
      data: JSON.stringify(loanDetails),
    })
      .then((res) => {
        console.log(res.data.message);
      }, console.log("apply loan failure"))
      .catch();

    e.preventDefault();
  }

  return (
    <Card>
      <Form noValidate validated={valid} onSubmit={submitLoan}>
        <Form.Group className="mb-3" name="loanType">
          <Form.Label>Loan Type</Form.Label>
          <Form.Select required onChange={(e) => setLoanType(e.target.value)}>
            <option>select loan</option>
            <option value='PersonalLoan'>PersonalLoan</option>
            <option>CarLoan</option>
            <option>PropertyLoan</option>
            <option>JumboLoan</option>
          </Form.Select>
          <Form.Control.Feedback type="invalid">
            Select your Loan type
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-3" name="name">
          <Form.Label>Loan Amount</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="Loan Amount"
            onChange={(e) => setLoanAmount(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Enter your loan Amount
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-3" name="address">
          <Form.Label>Loan Date</Form.Label>
          <Form.Control
            required
            type="date"
            placeholder="Loan Date"
            onChange={(e) => setAppliedDate(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Enter Date
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-3" name="state">
          <Form.Label>Rate of interest</Form.Label>
          <Form.Control
            required
            type="number"
            placeholder="Rate of Interest"
            onChange={(e) => setRateOfInterest(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please enter rate of interest
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-3" name="country">
          <Form.Label>Loan Duration</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="Loan Duration"
            onChange={(e) => setLoanDuration(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please enter your duration of loan
          </Form.Control.Feedback>
        </Form.Group>
        <Button variant="primary" type="submit" className="button-spacing">
          Submit
        </Button>
        <Button variant="warning" type="reset">
          Reset
        </Button>
      </Form>
    </Card>
  );
}
