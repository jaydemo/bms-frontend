import axios from "axios";
import { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { NavLink, useNavigate } from "react-router-dom";
import Card from "../UI/Card";
import "./Register.css";
function Register() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [state, setState] = useState("");
  const [country, setCountry] = useState("");
  const [email, setEmail] = useState("");
  const [pan, setPAN] = useState("");
  const [contactNumber, setContactNumber] = useState("");
  const [dob, setDOB] = useState(new Date());
  const [accountType, setAccountType] = useState("");
  const navigate = useNavigate();
  const [valid, setValidity] = useState(false);
  function submit(e) {
    let customerDetails = {
      username,
      password,
      name,
      address,
      state,
      country,
      email,
      pan,
      contactNumber,
      dob,
      accountType,
    };
    console.info(customerDetails);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.preventDefault();
      e.stopPropagation();
    }
    setValidity(true);
    axios({
      headers: {
        "Content-Type": "application/json",
      },
      method: "post",
      url: "http://localhost:9090/loginapp/registerUser",
      data: JSON.stringify(customerDetails),
    })
      .then((res) => {
        console.log(res.data.message);
        navigate("/login");
      }, console.log("did not receive any token"))
      .catch();

    e.preventDefault();
  }
  // const isPasswordValid = (e) => {
  //     const passwordPattern = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/;
  //     return passwordPattern.test(e.target.value);
  // }
  return (
    <Card>
      <Form noValidate validated={valid} onSubmit={submit}>
        <Form.Group className="mb-3" name="username">
          <Form.Label>Username</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="Enter your username"
            onChange={(e) => setUsername(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please choose a username
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group className="mb-3" name="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            required
            type="password"
            placeholder="Password"
            onChange={(e) => {
              setPassword(e.target.value);
              console.log(password);
            }}
          />
          <Form.Control.Feedback type="invalid">
            Please choose a password
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-3" name="name">
          <Form.Label>Name</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="Name"
            onChange={(e) => setName(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please enter name
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-3" name="address">
          <Form.Label>Address</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="Address"
            onChange={(e) => setAddress(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please enter your address
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-3" name="state">
          <Form.Label>State</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="State"
            onChange={(e) => setState(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please enter your state
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-3" name="country">
          <Form.Label>Country</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="Country"
            onChange={(e) => setCountry(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please enter your country
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-3" name="email">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            required
            type="email"
            placeholder="Enter email"
            onChange={(e) => setEmail(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please enter email
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-3" name="pan">
          <Form.Label>PAN</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="PAN"
            onChange={(e) => setPAN(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please enter PAN
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-3" name="contactNumber">
          <Form.Label>Contact Number</Form.Label>
          <Form.Control
            required
            type="number"
            placeholder="Contact Number"
            onChange={(e) => setContactNumber(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please enter contact
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-3" name="dob">
          <Form.Label>Date Of Birth</Form.Label>
          <Form.Control
            required
            type="date"
            placeholder="Date Of Birth"
            onChange={(e) => setDOB(e.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please enter your date of birth
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Account Type</Form.Label>
          <Form.Select onChange={(e) => setAccountType(e.target.value)}>
            <option>Savings</option>
            <option>Current</option>
          </Form.Select>
          <Form.Control.Feedback type="invalid">
            Please select account type
          </Form.Control.Feedback>
        </Form.Group>
        <Button variant="primary" type="submit" className="button-spacing">
          Submit
        </Button>
        <Button variant="warning" type="reset">
          Reset
        </Button>
      </Form>
      Already have an account?
      <NavLink to="/login" className="link-decoration-login text-primary">
        {" "}
        Login here
      </NavLink>
    </Card>
  );
}

export default Register;
